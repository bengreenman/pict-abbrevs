#lang scribble/manual

@require[
  scribble/example
  pict-abbrevs
  (for-label
    pict
    pict/convert
    ppict/2
    pict-abbrevs
    pict-abbrevs/arrow
    plot/no-gui
    racket/base
    racket/class
    racket/contract
    racket/draw
    racket/math
    (only-in slideshow/base current-slide-assembler margin client-w client-h))]

@(define x-eval (make-base-eval '(require pict pict-abbrevs ppict/tag)))
@(define (make-eval) x-eval)

@title{Pict Abbrevs}

@defmodule[pict-abbrevs]{
  The @racketmodname[pict-abbrevs] module exports the following bindings and
   does not depend on @racketmodname[racket/gui/base].

  @history[#:changed "0.3"
          @elem{The @racketmodname[pict-abbrevs] module no longer reprovides
                @racketmodname[pict-abbrevs/slideshow], and consequently no
                longer depends on @racketmodname[racket/gui/base].}]
}


@section{Pict Utilities}

@defthing[#:kind "value" revolution real?]{
  Equal to @racket[(* 2 pi)], or @hyperlink["https://tauday.com/tau-manifesto"]{the tau constant}.
  Useful for rotating picts.
  @margin-note{If the name @litchar{revolution} is too long, then do @racket[(require (rename-in pict-abbrevs [revolution turn]))].}

  @examples[#:eval (make-eval)
    (arrowhead 30 (*   0 revolution))
    (arrowhead 30 (* 1/4 revolution))
    (arrowhead 30 (* 1/2 revolution))
  ]
}

@defthing[#:kind "contract" real% flat-contract?]{
  Contract for "real percent".
  Same as @racket[(between/c 0 1)].
}

@defthing[#:kind "contract" nonnegative-real? flat-contract?]{
  Same as @racket[(>=/c 0)].

  @examples[#:eval (make-eval)
    (nonnegative-real? 0)
    (nonnegative-real? 2.77)
    (nonnegative-real? 9001)
    (nonnegative-real? -1)
    (nonnegative-real? 'X)]
}

@defthing[#:kind "contract" pict-color/c (-> any/c boolean?)]{
  Flat contract for the kinds of values that @racketmodname[pict] functions
   can usually interpret as colors.
}

@defproc[(pict-color->color% [pc pict-color/c] [#:default default pict-color/c]) (is-a?/c color%)]{
  Creates a @racket[color%] object.
  If @racket[pc] and @racket[default] are @racket[#false], returns @racket[(string->color% "white")].

  @examples[#:eval (make-eval)
    (pict-color->color% "blue")
    (pict-color->color% #f)]
}

@defthing[#:kind "contract" rgb-triplet/c (-> any/c boolean?)]{
  Flat contract for an RGB triplet.
  See also: @secref["utils" #:doc '(lib "plot/scribblings/plot.scrbl")].
}

@defproc[(rgb-triplet->color% [c rgb-triplet/c]) (is-a?/c color%)]{
  Converts an RGB triplet to a color object.
}

@defproc[(hex-triplet->color% [n (integer-in #x000000 #xffffff)]) (is-a?/c color%)]{
  Converts a Hex Code RGB to a color object.
  Expects inputs between @litchar{#x000000} and @litchar{#xffffff}.
}
@margin-note{@url{https://image-color.com} can select hex colors from an image.}

@deftogether[(
  @defthing[#:kind "value" racket-red (is-a?/c color%) #:value (hex-triplet->color% (string->number "#x9F1D20"))]{}
  @defthing[#:kind "value" racket-blue (is-a?/c color%) #:value (hex-triplet->color% (string->number "#x3E5BA9"))]{}
)]{
  Colors from Matthew Butterick's @hyperlink["https://en.wikipedia.org/wiki/File:Racket-logo.svg"]{Racket logo}.

  @examples[#:eval (make-eval)
    (ht-append 10
               (disk 20 #:color racket-red)
               (disk 20 #:color racket-blue))]
}

@defproc[(color%-update-alpha [c (is-a?/c color%)] [a (real-in 0 1)]) (is-a?/c color%)]{
  @examples[#:eval (make-eval)
    (define red (string->color% "red"))
    (disk 20 #:color red)
    (disk 20 #:color (color%-update-alpha red 0.3))]
}

@defproc[(color%++ [c (is-a?/c color%)] [a exact-integer?]) (is-a?/c color%)]{
  Add a number to every component (R,G,B) of a color.

  @examples[#:eval (make-eval)
    (define red (string->color% "red"))
    (disk 20 #:color red)
    (disk 20 #:color (color%++ red 40))
    (disk 20 #:color (color%++ red -40))]
}

@deftogether[(
  @defproc[(pict-bbox-sup [p pict?] ...) (listof pict?)]
  @defproc[(pict-bbox-sup* [p* (listof pict?)]) (listof pict?)]
)]{
  Returns a list of picts with identical width and height.
  The new picts may be old picts superimposed upon a blank background.

  @examples[#:eval (make-eval)
    (map frame (pict-bbox-sup (disk 10) (disk 20) (disk 5)))]
}

@defproc[(max* [r* (listof real?)]) real?]{
  Similar to @racket[max] but expects a list of numbers.
  Raises an @racket[exn:fail:contract?] error when given an empty list.

  @examples[#:eval (make-eval)
    (max* '(8 6 7))]
}

@defproc[(min* [r* (listof real?)]) real?]{
  Similar to @racket[min] but expects a list of numbers.
  Raises an @racket[exn:fail:contract?] error when given an empty list.

  @examples[#:eval (make-eval)
    (min* '(8 6 7))]
}

@defproc[(midpoint [x0 real?] [x1 (>=/c x0)]) real?]{
  Compute the midpoint of the interval @tt{[}@racket[x0]@tt{, }@racket[x1]@tt{]}.

  @examples[#:eval (make-eval)
    (midpoint 10 20)]
}

@defproc[(rule [w real?] [h real?] [#:color c pict-color/c "black"]) pict?]{
  Returns a @racket[filled-rectangle] with the given width, height, and color.

  @examples[#:eval (make-eval)
    (rule 20 2)
    (rule 1 10)
    (rule 8 8)]
}

@defproc[(string->color% [str string?]) (is-a?/c color%)]{
  Converts a string to a color object.
}

@defproc[(save-pict [ps path-string?] [p pict?] [kind (or/c 'png 'jpeg 'xbm 'xpm 'bmp 'pdf 'ps 'eps) 'png]) boolean?]{
  Exports the given pict to the file @racket[ps], using @racket[kind] to determine the output format.

  @history[#:changed "0.8"
          @elem{Added support for @racket['pdf], @racket['ps], @racket['eps].
          Thanks to
          @hyperlink["https://github.com/racket/pict/issues/74"]{@tt{racket/pict/issues/74}}.}]
}

@deftogether[(
  @defproc[(scale-to-width [pp pict?] [w real?]) pict?]
  @defproc[(scale-to-height [pp pict?] [h real?]) pict?]
  @defproc[(scale-to-square [pp pict?] [len real?]) pict?]
  @defproc[(scale-to-pict [pp pict?] [frame pict?]) pict?]
)]{
  Variants of @racket[scale-to-fit].

  @history[#:added "0.8"]
}

@defproc[(add-rectangle-background [pp pict?]
                                   [#:radius radius real? 10]
                                   [#:color color pict-color/c "white"]
                                   [#:draw-border? draw-border? boolean? #false]
                                   [#:x-margin x-margin real? 0]
                                   [#:y-margin y-margin real? 0]) pict?]{
  Add a rectangle behind a pict.

  @examples[#:eval (make-eval)
    (add-rectangle-background (standard-fish 100 50) #:color "bisque")]
}

@defproc[(add-rounded-border [pp pict?]
                             [#:radius radius real? 10]
                             [#:background-color bg-color pict-color/c "white"]
                             [#:frame-width frame-width real? 1]
                             [#:frame-color frame-color pict-color/c "black"]
                             [#:x-margin x-margin real? 0]
                             [#:y-margin y-margin real? 0]) pict?]{
  Add a bordered rectangle behind a pict.

  @examples[#:eval (make-eval)
    (add-rounded-border (standard-fish 100 50) #:x-margin 20 #:y-margin 30)]
}

@defproc[(add-spotlight-background [pp pict?]
                                   [#:blur blur (or/c #f real?) 15]
                                   [#:border-color border-color pict-color/c "plum"]
                                   [#:color color pict-color/c border-color]
                                   [#:border-width border-width real? 10]
                                   [#:x-margin x-margin real? 40]
                                   [#:y-margin y-margin real? 40]) pict?]{
  Superimposes the given pict on a blurred ellipse.

  @examples[#:eval (make-eval)
    (add-spotlight-background (jack-o-lantern 80))
    (add-spotlight-background (jack-o-lantern 80)
                              #:border-color "firebrick"
                              #:color (color%-update-alpha (string->color% "white") 0)
                              #:border-width 15
                              #:x-margin 30
                              #:y-margin 5)]
}

@defproc[(bbox [pp pict?]
               [#:color color pict-color/c "white"]
               [#:frame-color frame-color (or/c #f pict-color/c) (bbox-frame-color)]
               [#:frame-width frame-width (or/c #f real?) (bbox-frame-width)]
               [#:x-margin x-margin (or/c #f real?) (bbox-x-margin)]
               [#:y-margin y-margin (or/c #f real?) (bbox-y-margin)]) pict?]{
  A standard big box, or basic box.
  Most importantly, the name @tt{bbox} is easy to type.

  Comes with reasonable defaults and parameters for fine-tuning.

  @examples[#:eval (make-eval)
    (hc-append 10 (bbox (text "Hello World")) (bbox (desktop-machine 1)))]
}

@defproc[(sbox [pp pict?]) pict?]{
  Draws a small box around a pict. The name is easy to type.

  @examples[#:eval (make-eval)
    (hc-append 10 (sbox (text "Hello World")) (sbox (desktop-machine 1)))]
}

@deftogether[(
  @defparam[bbox-radius rad real? #:value 1]{}
  @defparam[bbox-x-margin xx real? #:value 66]{}
  @defparam[bbox-y-margin yy real? #:value 14]{}
  @defparam[bbox-frame-width fw real? #:value 2]{}
  @defparam[bbox-frame-color cc pict-color/c #:value (hex-triplet->color% #x002E6D)]{}
)]{
  Default values for @racket[bbox] and @racket[sbox] picts.
}

@defproc[(tag-append [x any/c] ...) symbol?]{
  Create a new tag (symbol) from a sequence of values.
  Equivalent to:
    @nested[#:style 'inset
      @racket[(string->symbol (string-join (map ~a x*) "-"))]]

  @examples[#:eval (make-eval)
    (tag-append 'N 'W)
    (tag-append "-" 1)]
}

@defproc[(add-hubs [pp pict?]
                   [tag symbol?]
                   [#:hub-length hub-len (or/c nonnegative-real? #f) #f]
                   [#:margin margin (or/c nonnegative-real? #f) #f]) pict?]{
Add four blank picts to the sides of the base pict.
These blank "hubs" are useful targets for lines and/or arrows; see the examples below.

@itemlist[
@item{
Each hub has a tag based on its compass direction:
 @racket[(tag-append tag 'N)] for the top hub,
 @racket[(tag-append tag 'E)] for the right hub,
 @racket[(tag-append tag 'S)] for the bottom hub, and
 @racket[(tag-append tag 'W)] for the left hub.
}
@item{
The @racket[hub-len] sets the size of each hub.
Imagine four invisible lines around the pict, one on each side, each one
centered in the middle of the side and @racket[hub-len] units long.
}
@item{
The @racket[margin] sets the distance between a hub and
the edge of the base pict.
If @racket[#f], the default is a small positive distance.
}
]

@examples[#:eval (make-eval) #:label "Non-Example:"
  (let ((pp (ht-append 40
              (tag-pict (disk 30 #:color "pink") 'A)
              (tag-pict (disk 30 #:color "peru") 'B))))
    (pin-line pp
      (find-tag pp 'A) rc-find
      (find-tag pp 'B) lc-find))]
@examples[#:eval (make-eval)
  (let ((pp (ht-append 40
              (add-hubs (disk 30 #:color "pink") 'A)
              (add-hubs (disk 30 #:color "peru") 'B))))
    (pin-line pp
      (find-tag pp 'A-E) rc-find
      (find-tag pp 'B-W) lc-find))
  (let ((pp (ht-append 40
              (add-hubs (disk 30 #:color "pink") 'A)
              (add-hubs (disk 30 #:color "peru") 'B))))
    (pin-arrow-line 9 pp
      (find-tag pp 'A-E) rt-find
      (find-tag pp 'B-W) lb-find
      #:start-angle (* 1/8 revolution)
      #:end-angle (* 1/8 revolution)
      #:start-pull 5/4
      #:end-pull 5/4))
]}

@defproc[(clip-to [pp pict?]
                  [width nonnegative-real?]
                  [height nonnegative-real?]) pict?]{
  Clips a pict to a box with the given width and height
  (positioned at the center of the pict).

@examples[#:eval (make-eval)
  (clip-to (disk 50) 40 30)
]}


@subsection{Pict Constructors}

@defproc[(pblank [base pict?]) pict?]{
  Draw a blank pict with the same width and height as the given pict.
  @examples[#:eval (make-eval)
    (let* ((p0 (filled-rectangle 30 30))
           (p1 (pblank p0))
           (sep 2)
           (r0 (ht-append sep p0 p1 p0))
           (r1 (ht-append sep p1 p0 p1)))
      (vc-append sep r0 r1))]

  The pict function @racket[ghost] is similar, but preserves tags and
  other metadata.
}

@defproc[(xblank [width real?]) pict?]{
  Draw a blank pict with the given width and zero height.
  Same as @racket[(blank width 0)].

  @history[#:added "0.7"]
}

@defproc[(yblank [height real?]) pict?]{
  Draw a blank pict with the given height and zero width.
  Same as @racket[(blank 0 height)].

  @history[#:added "0.7"]
}

@defproc[(ptable [pict-tree (listof (or/c pict? pair? list?))]
                 [#:ncols ncols (or/c natural? #f) 2]
                 [#:col-sep col-sep (or/c nonnegative-real? #f) 20]
                 [#:row-sep row-sep (or/c nonnegative-real? #f) 10]
                 [#:col-align col-align (or/c procedure? list? #f) lc-superimpose]
                 [#:row-align row-align (or/c procedure? list? #f) cc-superimpose])
                 pict?]{
  Wrapper for the @racket[pict] table function.
  Accepts either a flat list of picts or a list of equal-length lists.

  @examples[#:eval (make-eval)
    (define (rect color-name)
      (filled-rounded-rectangle 30 20 2 #:color color-name #:draw-border? #f))
    (define pairs
      (for/list ((color-name (in-list '("aquamarine" "powder blue" "plum"))))
        (cons (text color-name)
              (rect color-name))))
    (ptable pairs)]

  @history[#:added "0.8"]
}

@defproc[(make-envelope-pict [w nonnegative-real?]
                             [h nonnegative-real?]
                             [#:color color pict-color/c "mint cream"]
                             [#:line-width line-width nonnegative-real? 2]
                             [#:line-color line-color pict-color/c "black"]) pict?]{
  Draw an envelope.

  @examples[#:eval (make-eval)
    (make-envelope-pict 28 20)
    (make-envelope-pict 50 24 #:color "rosy brown" #:line-color "powder blue" #:line-width 4)]

  @history[#:added "0.8"]
}

@deftogether[(
  @defproc[(make-check-pict [n nonnegative-real?]
                            [#:color color pict-color/c #f]
                            [#:line-width line-width nonnegative-real? #f]
                            [#:line-color line-color pict-color/c #f]) pict?]
  @defproc[(make-cross-pict [n nonnegative-real?]
                            [#:color color pict-color/c #f]
                            [#:line-width line-width nonnegative-real? #f]
                            [#:line-color line-color pict-color/c #f]) pict?]
)]{
  Draw a check mark, draw an X mark.

  @examples[#:eval (make-eval)
    (ht-append 4
      (make-check-pict 40)
      (make-cross-pict 40))]

  @history[#:added "0.8"]
}

@defproc[(make-compass-pict [side-len nonnegative-real?] [#:color color pict-color/c "black"]) pict?]{
  Draw a 4-pointed star that fits in a square with side length @racket[side-len].

  @examples[#:eval (make-eval)
    (make-compass-pict 10)
    (make-compass-pict 20)]
}

@defproc[(make-mouse-cursor-pict [w nonnegative-real?] [h nonnegative-real?] [#:color color pict-color/c "black"]) pict?]{
  Draw a simple mouse cursor.

  @examples[#:eval (make-eval)
    (make-mouse-cursor-pict 20 32)]
}

@defproc[(make-simple-flag [base pict?]
                           [#:flag-background-color flag-background-color (or/c (is-a?/c color%) #f) #f]
                           [#:flag-border-color flag-border-color (or/c (is-a?/c color%) #f) #f]
                           [#:flag-border-width flag-border-width (or/c nonnegative-real? #f) #f]
                           [#:flag-brush-style flag-brush-style (or/c brush-style/c #f) #f]
                           [#:flag-x-margin flag-x-margin (or/c nonnegative-real? #f) #f]
                           [#:flag-y-margin flag-y-margin (or/c nonnegative-real? #f) #f]
                           [#:pole-width pole-width (or/c nonnegative-real? #f) #f]
                           [#:pole-height pole-height (or/c nonnegative-real? #f) #f]
                           [#:pole-color pole-color (or/c (is-a?/c color%) #f) #f]
                           [#:pole-border-color pole-border-color (or/c (is-a?/c color%) #f) #f]) pict?]{
  Draw a simple flag with @racket[base] superimposed on the fabric.

  Increase @racket[flag-x-margin] and @racket[flag-y-margin] to add space between the base pict and the edge of the flag.

  @examples[#:eval (make-eval)
    (make-simple-flag (standard-fish 80 40 #:direction 'right))]
}

@defproc[(lightbulb-pict
           [#:color         color (or/c string? (is-a?/c color%) (is-a?/c brush%)) "yellow"]
           [#:base-color    base-color (or/c string? (is-a?/c color%)) (make-color 200 200 200)]
           [#:border-color  border-color (or/c string? (is-a?/c color%)) (make-color 0 0 0)]
           [#:tip-color     tip-color (or/c string? (is-a?/c color%)) border-color]
           [#:border-width  border-width (real-in 0 255) 2.5]
           [#:bulb-radius   bulb-radius (and/c rational? (not/c negative?)) 50]
           [#:stem-width-radians stem-width-radians (and/c rational? (not/c negative?)) (* pi 1/4)]
           [#:stem-height stem-height (and/c rational? (not/c negative?)) 15]
           [#:base-segments base-segments natural-number/c 3]
           [#:base-segment-height base-segment-height (and/c rational? (not/c negative?)) 9]
           [#:base-segment-corner-radius base-segment-corner-radius real? 3]
           [#:tip-ratio tip-ratio (and/c rational? (not/c negative?)) 5/12])
          pict?]{
  Standard lightbulb.
  By @hyperlink["https://gist.github.com/LiberalArtist/4d0059f5af23043515a3cc74bd4928c2"]{Phil McGrath}.
  License @tt{Apache-2.0}.

  @examples[#:eval (make-eval)
    (lightbulb-pict)]

  @history[#:added "0.11"]
}

@defproc[(make-font-table-pict [example-str string?] [#:size font-size (or/c (integer-in 1 1024) #f) #f] [#:limit n (or/c exact-nonnegative-integer? #f) #f]) pict?]{
  Draw the given string using all fonts returned by @racket[(get-face-list)],
   or the first @racket[n] fonts if a limit is supplied.

  @examples[#:eval (make-eval)
    (make-font-table-pict "Racket" #:limit 6)]
}

@section{Arrow Abbrevs}

@defmodule[pict-abbrevs/arrow]{
  Tools for describing arrows and adding arrows to picts.

  @history[#:added "0.12"]
}

@defstruct[parrow ([src-tag (or/c symbol? pict-path?)]
                   [src-find (pict-convertible? pict-path? . -> . (values real? real?))]
                   [tgt-tag (or/c symbol? pict-path?)]
                   [tgt-find (pict-convertible? pict-path? . -> . (values real? real?))]
                   [start-angle real?]
                   [end-angle real?]
                   [start-pull real?]
                   [end-pull real?]
                   [style (or/c 'transparent 'solid 'xor 'hilite 'dot 'long-dash 'short-dash 'dot-dash 'xor-dot 'xor-long-dash 'xor-short-dash 'xor-dot-dash #f)])]{
  Data definition corresponding to several arguments for @racket[pin-arrow-line] and @racket[pin-line].
}

@deftogether[(
  @defparam[*parrow-line-width* line-width real? #:value 4]{}
  @defparam[*parrow-arrow-size* arrow-size real? #:value 14]{}
  @defparam[*parrow-color* cc (is-a?/c color%) #:value "black"]{}
)]{
  Defaults for line size, arrowhead size, and line & arrow color.
}

@defproc[(add-parrow [base-pict pict?]
                     [arrow parrow?]
                     [#:double-head? double-head? any/c #f]
                     [#:arrow-size arrow-size real? (*parrow-arrow-size*)]
                     [#:line-width line-width real? (*parrow-line-width*)]
                     [#:color color (is-a?/c color%) (*parrow-color*)]
                     [#:label label pict? (blank)]
                     [#:x-adjust-label x-sep real? 0]
                     [#:y-adjust-label y-sep real? 0]
                     [#:hide? hide? any/c #f]) pict?]{
  Adds one arrow to a pict.

  @examples[#:eval (make-base-eval '(require pict pict-abbrevs ppict/tag pict-abbrevs/arrow))
    (require pict-abbrevs/arrow)
    (define pict-a (add-hubs (rectangle 40 40) 'A))
    (define pict-b (add-hubs (circle 40) 'B))
    (define above-arrow (parrow 'A-N ct-find 'B-N ct-find (* 1/4 revolution) (* 3/4 revolution) 1/4 1/4 'solid))
    (define below-arrow (parrow 'A-S cb-find 'B-S cb-find (* 3/4 revolution) (* 1/4 revolution) 2/4 2/4 'long-dash))
    (add-parrow
      (add-parrow (hc-append pict-a (blank 80 200) pict-b) above-arrow)
      below-arrow)]
}

@defproc[(add-pline  [base-pict pict?]
                     [arrow parrow?]
                     [#:line-width line-width real? (*parrow-line-width*)]
                     [#:color color (is-a?/c color%) (*parrow-color*)]
                     [#:label label pict? (blank)]
                     [#:x-adjust-label x-sep real? 0]
                     [#:y-adjust-label y-sep real? 0]
                     [#:hide? hide? any/c #f]) pict?]{
  Adds one line to a pict.

  @examples[#:eval (make-base-eval '(require pict pict-abbrevs ppict/tag pict-abbrevs/arrow))
    (add-pline (hc-append 80 (tag-pict (rectangle 40 40) 'A) (tag-pict (circle 40) 'B))
      (parrow 'A rc-find 'B lc-find 0 0 1 1 'solid)
      #:label (text "from rectangle to circle")
      #:y-adjust-label -10) ]
}

@deftogether[(
  @defproc[(add-parrows [base-pict pict?] [#:arrow-size arrow-size (*parrow-arrow-size*)] [#:color color (is-a?/c color%)] [arrows (listof parrow?)] ... ) pict?]{}
  @defproc[(add-parrows* [base-pict pict?] [arrows (listof parrow?)] [#:arrow-size arrow-size (*parrow-arrow-size*)] [#:color color (is-a?/c color%)]) pict?]{}
  @defproc[(add-parrow*  [base-pict pict?] [arrows (listof parrow?)] [#:arrow-size arrow-size (*parrow-arrow-size*)] [#:color color (is-a?/c color%)]) pict?]{}
  @defproc[(add-plines [base-pict pict?] [#:color color (is-a?/c color%)] [lines (listof parrow?)] ...) pict?]{}
  @defproc[(add-pline* [base-pict pict?] [lines (listof parrow?)] [#:color color (is-a?/c color%)]) pict?]{}
)]{
  Add several arrows or lines to a base pict.
}


@section{LTL Abbrevs}

@defmodule[pict-abbrevs/ltl]{
  Drawing tools related to linear temporal logic (LTL).

  @history[#:added "0.12"]
}

@defproc[(trace-pict [sym** (listof (listof (or/c 'R 'G 'B)))]
                     [#:index-labels? index-labels any/c #true]
                     [#:lasso? lasso? any/c #true]) pict?]{
  @examples[#:eval (make-base-eval '(require pict pict-abbrevs ppict/tag pict-abbrevs/ltl))
    (trace-pict '((R) (G) (B)))
    (trace-pict '((R G B) (G)) #:lasso? #f)]
}


@section{Slideshow Abbrevs}

@defmodule[pict-abbrevs/slideshow]{
  Helpers for working with @racketmodname[slideshow] or @racketmodname[ppict/2].
  This module depends on @racketmodname[racket/gui/base].
}

@defthing[#:kind "contract" slide-assembler/c chaperone-contract?]{
  Contract for a function that can be used to build @racketmodname[slideshow] slides.
  See also @racket[current-slide-assembler].
}

@defproc[(slide-assembler/background [base-assembler slide-assembler/c]
                                     [#:color background-color pict-color/c]
                                     [#:draw-border? draw-border? boolean? #false]
                                     [#:border-color border-color pict-color/c #false]
                                     [#:border-width border-width (or/c #f real?) #false]) slide-assembler/c]{
  Returns a slide assembler that: (1) uses the given @racket[base-assembler] to
  create a pict, and (2) superimposes the pict onto a @racket[filled-rectangle]
  that covers the screen.
  The optional arguments set the style of the background rectangle.

  @codeblock|{
    #lang racket/base
    (require pict-abbrevs/slideshow slideshow)

    (parameterize ((current-slide-assembler
                     (slide-assembler/background
                       (current-slide-assembler)
                       #:color "red"))
                   (current-font-size 60))
      (slide (t "HOLA")))
  }|
}

@defproc[(pixels->w% [x nonnegative-real?]) real%]{
  Converts a pixel distance to a percentage of the max screen width, i.e., @racket[(+ (* 2 margin) client-w)].
  Raise an @racket[exn:fail:contract?] exception if the given distance exceeds the max width.
}

@defproc[(pixels->h% [x nonnegative-real?]) real%]{
  Converts a pixel distance to a percentage of the max screen height, i.e., @racket[(+ (* 2 margin) client-h)].
  Raise an @racket[exn:fail:contract?] exception if the given distance exceeds the max height.
}

@defproc[(w%->pixels [w real%]) nonnegative-real?]{
  Converts a percent to the number of pixels required to cover that percent
   of @racket[client-w].

  @codeblock|{
    #lang racket/base
    (require slideshow/base pict-abbrevs/slideshow)

    (w%->pixels 1/10)
    (w%->pixels 5/10)
    (= client-w (w%->pixels 1))}|
}

@defproc[(h%->pixels [w real%]) nonnegative-real?]{
  Converts a percent to the number of pixels required to cover that percent
   of @racket[client-h].
}

@defproc[(text/color [str string?] [c pict-color/c]) pict?]{
  Draws colored text.

  @codeblock|{
    #lang racket/base
    (require pict-abbrevs/slideshow)

    (text/color "red" "red")}|
}

@deftogether[(
  @defproc[(at-underline [pp (or/c tag-path? pict-path?)] [#:abs-x abs-x real?] [#:abs-y abs-y real?]) refpoint-placer?]
  @defproc[(at-leftline [pp (or/c tag-path? pict-path?)] [#:abs-x abs-x real?] [#:abs-y abs-y real?]) refpoint-placer?]
)]{
  Returns a placer that places picts to a reference point relative to an existing
  pict within the base.
  For @racket[at-underline] the reference point is the bottom-left.
  For @racket[at-leftline] the reference point is the top-left.
  If given, @racket[abs-x] and @racket[abs-y] shift the reference point.
  See also @racket[at-find-pict].
}

@defproc[(make-underline [pp (or/c pict? real?)] [#:height height real?] [#:color color pict-color/c] [#:width width #f (or/c #f real?)]) pict?]{
  Draw a horizontal line wide enough to underline the given pict.

  @codeblock|{
    #lang racket/base
    (require pict-abbrevs/slideshow ppict/2)

    (let ((word (text "Word")))
      (ppict-do
        (file-icon 50 40 "bisque")
        #:go (coord 1/2 1/2 'cc)
        word
        #:go (at-underline word)
        (make-underline word)))}|
}

@defproc[(make-leftline [pp (or/c pict? real?)] [#:height height real?] [#:color color pict-color/c] [#:width width #f (or/c #f real?)]) pict?]{
  Draw a vertical line that is equally high as the given pict.

  @codeblock|{
    #lang racket/base
    (require pict-abbrevs/slideshow ppict/2)

    (let ((word (text "Word")))
      (ppict-do
        (file-icon 100 80 "bisque")
        #:go (coord 1/2 1/2 'cc)
        word
        #:go (at-leftline word)
        (make-leftline word #:width 10)))}|
}

@defproc[(make-highlight* [pp pict?] [tag symbol?] [#:color color pict-color/c]) pict?]{
  Add a background of the given color to all picts tagged (in the sense of
  @racket[tag-pict?]) with @racket[tag] in the scene @racket[pp].

  @codeblock|{
    #lang racket/base
    (require pict-abbrevs/slideshow ppict/2)

    (ppict-do
      (blank 80 40)
      #:set (for/fold ((acc ppict-do-state))
                      ((i (in-range 8)))
              (ppict-do
                acc
                #:go (coord (/ (* i 10)  80) 9/10)
                (if (even? i)
                  (tag-pict (text "X") 'X)
                  (tag-pict (text "O") 'O))))
      #:set (make-highlight* ppict-do-state 'X))}|
}

@defthing[#:kind "value" highlight-pen-color pict-color/c]{
  Default color for underlines, etc.
}

@defthing[#:kind "value" highlight-brush-color pict-color/c]{
  Default color for highlights.
}


@section{PPict Abbrevs}

@defmodule[pict-abbrevs/pplay]{
  Animation helper for ppicts.

  Origin: @url{https://github.com/rmculpepper/ppict/pull/6}
}

@defproc[(pplay [gen (-> ppict? (real-in 0.0 1.0) pict?)]
                [#:steps steps exact-positive-integer? (current-play-steps)]
                [#:delay delay-secs real? 0.05]
                [#:skip-first? skip-first? any/c #f]
                [#:title title (or/c string? #f) #f]
                [#:name name (or/c string? #f) title]
                [#:aspect aspect aspect? #f]
                [#:layout layout (or/c 'auto 'center 'top 'tall) 'auto]
                [#:gap-size real? (current-gap-size)]
                [#:inset slide-inset? no-inset])
         void?]{
Generates @math{@racket[steps]+1} pslides by calling @racket[gen] on a base
ppict (created by @racket[pslide]) and equally-spaced values from @racket[0.0]
(inclusive) to @racket[1.0] (exclusive).
Except for the first slide (which may be skipped), each slide has a timeout of
@racket[delay-secs], so that the next slide appears automatically.

The @racket[#:steps], @racket[#:delay], and @racket[#:skip-first?]
options are interpreted the same as for the @racket[play]
procedure.
The remaining options are interpreted the same as for
@racket[pslide].

Example:
@racketblock[
  (pplay
    (lambda (pp n)
      (ppict-do
        pp
        #:go (coord 1/2 1/2)
        (cellophane (text "HELLO") n))))
]

  @history[#:added "0.8"]
}


@section{raco pict}

To vertically append image files and/or Racket modules:

@exec{raco pict vl-append ARG ...}

If an @litchar{ARG} is an image file, then @tt{raco pict} parses it via the
 @racket[bitmap] function.

If an @litchar{ARG} is a @hash-lang[] module,
then it must contain a submodule named @tt{raco-pict}
that provides an identifier named @tt{raco-pict}.
For example:

@codeblock|{
#lang racket/base
(module+ raco-pict
  (require pict)
  (provide raco-pict)
  (define raco-pict (disk 40)))
}|

Other @racketmodname[pict] functions may work.

Other arguments may work as expected.
Certainly @tt{raco pict vl-append 20 a.png b.png} vertically appends two image
 files with "20 space" in between.
