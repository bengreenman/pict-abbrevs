#lang racket

(provide
  pplay)

(require
  (only-in slideshow/base current-gap-size make-slide-inset condense? skip-slides)
  (only-in slideshow/play current-play-steps)
  (only-in ppict/2 pslide ppict-do-state))

;; ---

(define no-inset (make-slide-inset 0 0 0 0))

(define (pplay gen
               #:steps [N (current-play-steps)]
               #:delay [secs 0.05]
               #:skip-first? [skip-first? #f]
               #:title [title #f]
               #:name [name title]
               #:aspect [aspect #f]
               #:layout [layout 'auto]
               #:gap-size [gap-size (current-gap-size)]
               #:inset [inset no-inset])
  (define-syntax-rule (pslide+ timeout n)
    (pslide #:title title
            #:name name
            #:aspect aspect
            #:layout layout
            #:gap-size gap-size
            #:inset inset
            #:timeout timeout
            #:set (gen ppict-do-state n)))
  (unless skip-first?
    (pslide+ #f 0))
  (if condense?
      (skip-slides N)
      (for ([n (in-list
                (let ([cnt N])
                  (let loop ([n cnt])
                    (if (zero? n)
                        null
                        (cons (/ (- cnt -1 n) 1.0 cnt)
                              (loop (sub1 n)))))))])
        (pslide+ secs n))))
