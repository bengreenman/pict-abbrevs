#lang racket/base

(require racket/contract)
(provide
  lightbulb-pict
  nonnegative-real?
  real%
  racket-red
  racket-blue
  pict-abbrevs-logger
  log-pict-abbrevs-debug
  log-pict-abbrevs-info
  log-pict-abbrevs-warning
  log-pict-abbrevs-error
  log-pict-abbrevs-fatal
  tag-append
  bbox-radius
  bbox-x-margin
  bbox-y-margin
  bbox-frame-width
  bbox-frame-color
  bbox
  sbox
  (contract-out
    [revolution
      real?]
    [pict-color/c
      (-> any/c boolean?)]
    [pict-color->color%
      (->* [pict-color/c] [#:default pict-color/c] (is-a?/c color%))]
    [rgb-triplet/c
      (-> any/c boolean?)]
    [color%-update-alpha
      (-> (is-a?/c color%) (real-in 0 1) (is-a?/c color%))]
    [color%++
      (-> (is-a?/c color%) exact-integer? (is-a?/c color%))]
    [rgb-triplet->color%
      (-> rgb-triplet/c (is-a?/c color%))]
    [hex-triplet->color%
      (-> (integer-in #x000000 #xffffff) (is-a?/c color%))]
    [pict-bbox-sup
      (->* [] #:rest (listof pict?) (listof pict?))]
    [pict-bbox-sup*
      (-> (listof pict?) (listof pict?))]
    [max*
      (-> (listof real?) real?)]
    [min*
      (-> (listof real?) real?)]
    [midpoint
      (-> real? real? real?)]
    [rule
      (->* [real? real?] [#:color pict-color/c] pict?)]
    [string->color%
      (-> string? (is-a?/c color%))]
    [save-pict
     (->* [path-string? pict?] [(or/c 'png 'jpeg 'xbm 'xpm 'bmp 'pdf 'eps)] any/c)]
    [scale-to-height
      (-> pict? nonnegative-real? pict?)]
    [scale-to-width
      (-> pict? nonnegative-real? pict?)]
    [scale-to-square
      (-> pict? nonnegative-real? pict?)]
    [scale-to-pict
      (-> pict? pict? pict?)]
    [add-rectangle-background
      (->* [pict?] [#:radius real?
                    #:color pict-color/c
                    #:draw-border? boolean?
                    #:x-margin real?
                    #:y-margin real?] pict?)]
    [add-spotlight-background
      (->* [pict?] [#:blur (or/c #f real?)
                    #:color pict-color/c
                    #:border-color pict-color/c
                    #:border-width real?
                    #:x-margin real?
                    #:y-margin real?] pict?)]
    [add-rounded-border
      (->* [pict?] [#:radius real?
                    #:background-color pict-color/c
                    #:frame-width real?
                    #:frame-color pict-color/c
                    #:x-margin real?
                    #:y-margin real?] pict?)]
    [add-hubs
      (->* [pict? symbol?] [#:hub-length (or/c nonnegative-real? #f)
                            #:margin (or/c nonnegative-real? #f)] pict?)]
    [xblank
      (-> real? pict?)]
    [yblank
      (-> real? pict?)]
    [pblank
      (-> pict? pict?)]
    [ptable
      (->* [(listof (or/c pict? pair? list?))]
           [#:ncols (or/c exact-nonnegative-integer? #f)
            #:col-sep (or/c nonnegative-real? #f)
            #:row-sep (or/c nonnegative-real? #f)
            #:col-align (or/c procedure? list? #f)
            #:row-align (or/c procedure? list? #f)]
           pict?)]
    [make-envelope-pict
      (->* [nonnegative-real? nonnegative-real?]
           [#:color pict-color/c
            #:line-width nonnegative-real?
            #:line-color pict-color/c] pict?)]
    [make-check-pict
      (->* [nonnegative-real?]
           [#:color pict-color/c
            #:line-width nonnegative-real?
            #:line-color pict-color/c] pict?)]
    [make-cross-pict
      (->* [nonnegative-real?]
           [#:color pict-color/c
            #:line-width nonnegative-real?
            #:line-color pict-color/c] pict?)]
    [make-compass-pict
      (->* [nonnegative-real?] [#:color pict-color/c] pict?)]
    [make-mouse-cursor-pict
      (->* [nonnegative-real? nonnegative-real?] [#:color pict-color/c] pict?)]
    [make-simple-flag
      (->* [pict?] [#:flag-background-color (or/c (is-a?/c color%) #f)
                    #:flag-border-color (or/c (is-a?/c color%) #f)
                    #:flag-border-width (or/c nonnegative-real? #f)
                    #:flag-brush-style (or/c brush-style/c #f)
                    #:flag-x-margin (or/c nonnegative-real? #f)
                    #:flag-y-margin (or/c nonnegative-real? #f)
                    #:pole-width (or/c nonnegative-real? #f)
                    #:pole-height (or/c nonnegative-real? #f)
                    #:pole-color (or/c (is-a?/c color%) #f)
                    #:pole-border-color (or/c (is-a?/c color%) #f)] pict?)]
    [make-font-table-pict
      (->* [string?] [#:size (or/c (integer-in 1 1024) #f) #:limit (or/c exact-nonnegative-integer? #f)] pict?)]
    [clip-to
      (-> pict? nonnegative-real? nonnegative-real? pict?)]))

(require
  (only-in racket/class new send is-a? is-a?/c make-object)
  (only-in racket/draw brush% pen% dc-path% color% make-color
           the-color-database get-face-list brush-style/c ps-setup% post-script-dc% pdf-dc%
           current-ps-setup)
  (only-in racket/format ~a)
  (only-in racket/math pi)
  (only-in racket/path path-get-extension)
  (only-in racket/list flatten)
  (only-in racket/string string-join)
  (only-in ppict/pict ppict-do coord)
  (only-in ppict/tag tag-pict)
  (only-in pict/shadow blur)
  (only-in pict-abbrevs/private/lightbulb lightbulb-pict)
  pict)

;; =============================================================================

(define-logger pict-abbrevs)

(define revolution (* 2 pi))

(define real% (real-in 0 1))

(define nonnegative-real? (>=/c 0))

(define pict-color/c
  (or/c #f string? (is-a?/c color%)))

(define rgb-triplet/c
  (list/c real? real? real?))

(define (color%-update-alpha c a)
  (make-object color% (send c red) (send c green) (send c blue) a))

(define (byte-round n)
  (if (< n 0)
    0
    (if (< 255 n)
      255 n)))

(define (color%++ c n)
  (make-object color%
               (byte-round (+ (send c red) n))
               (byte-round (+ (send c green) n))
               (byte-round (+ (send c blue) n))
               (send c alpha)))

(define (rgb-triplet->color% rgb)
  (make-object color% (car rgb) (cadr rgb) (caddr rgb)))

(define (hex-triplet->color% x)
  (define-values [r g b]
    (values (arithmetic-shift x -16)
            (bitwise-and #x0000ff (arithmetic-shift x -8))
            (bitwise-and #x0000ff x)))
  (make-color r g b))

(define (string->color% str)
  (or (send the-color-database find-color str)
      (raise-argument-error 'string->color% "color-name?" str)))

(define black (string->color% "black"))
(define white (string->color% "white"))

(define racket-red  (hex-triplet->color% #x9F1D20))
(define racket-blue (hex-triplet->color% #x3E5BA9))

(define (pict-bbox-sup #:join [pre-join #f] . p*)
  (pict-bbox-sup* p*))

;; Superimpose given picts onto blank backgrounds, such that each pict in
;;  result has same width and height (the max w/h)
(define (pict-bbox-sup* #:join [pre-join #f] p*)
  (cond
   [(null? p*)
    '()]
   [else
    (define w (max* (map pict-width p*)))
    (define h (max* (map pict-height p*)))
    (define bg (blank w h))
    (define do-join (or pre-join cc-superimpose))
    (for/list ((p (in-list p*)))
      (do-join bg p))]))

(define (save-pict fn p [-kind 'png])
  (define kind (or -kind (ext->sym (path-get-extension fn))))
  (case kind
    ((png jpeg xbm xpm bmp)
     (save-bitmap fn (pict->bitmap p) kind))
    ((pdf)
     (save-pict-pdf fn p))
    ((eps)
     (save-pict-eps fn p))
    ((ps)
     (save-pict-ps fn p))
    (else
      (raise-argument-error 'save-pict "invalid output target"
                            "target" kind
                            "path" fn))))

(define (save-bitmap fn bm kind)
  (send bm save-file fn kind))

(define (save-pict-pdf fn pict)
  (printf "save pdf~n")
  (save-pict-ps fn pict 'pdf))

(define (save-pict-eps fn pict)
  (save-pict-ps fn pict 'eps))

(define (save-pict-ps fn pict [kind 'ps])
  (define ps-setup (new ps-setup%))
  (send ps-setup copy-from (current-ps-setup))
  (send ps-setup set-file fn)
  (send ps-setup set-margin 0 0)
  (send ps-setup set-scaling 1 1)
  (send ps-setup set-translation 0 0)
  (send ps-setup set-editor-margin 0 0)
  (define dc
    (parameterize ([current-ps-setup ps-setup])
      (new (case kind ((pdf) pdf-dc%) (else post-script-dc%))
           [width (pict-width pict)]
           [height (pict-height pict)]
           [interactive #f]
           [parent #f]
           [use-paper-bbox #f]
           [as-eps (case kind ((eps) #true) (else #f))]
           [output fn])))
  (send dc set-smoothing 'smoothed)
  (send dc start-doc "")
  (send dc start-page)
  (draw-pict pict dc 0 0)
  (send dc end-page)
  (send dc end-doc))

(define (ext->sym bb)
  (if bb
    (string->symbol
      (bytes->string/utf-8
        (subbytes bb 1)))
    'png))

(define (scale-to-width pp w)
  (scale-to-fit pp w (pict-height pp)))

(define (scale-to-height pp h)
  (scale-to-fit pp (pict-width pp) h))

(define (scale-to-square pp dim)
  (scale-to-fit pp dim dim))

(define (scale-to-pict pp frame)
  (scale-to-fit pp (pict-width frame) (pict-height frame)))

(define (max* n*)
  (if (null? n*)
    (raise-argument-error 'max* "non-empty list" n*)
    (reduce max n*)))

(define (min* n*)
  (if (null? n*)
    (raise-argument-error 'min* "non-empty list" n*)
    (reduce min n*)))

(define (midpoint x0 x1)
  (+ x0 (/ (- x1 x0) 2)))

(define (reduce f x*)
  (for/fold ((acc (car x*)))
            ((x (in-list (cdr x*))))
    (f acc x)))

(define (rule w h #:color [c black])
  (filled-rectangle w h #:color c #:draw-border? #false))

(define (pict-color->color% pc #:default [default #f])
  (cond
    [(is-a? pc color%)
     pc]
    [(string? pc)
     (string->color% pc)]
    [(not pc)
     (if default
       (pict-color->color% default #:default #false)
       (string->color% "white"))]
    [else
      (raise-argument-error 'pict-color->color% "pict-color/c" pc)]))

(define (add-rectangle-background p
                                  #:radius [radius 10]
                                  #:color [pre-color #false]
                                  #:draw-border? [draw-border? #false]
                                  #:x-margin [x-margin 0]
                                  #:y-margin [y-margin 0])
  (define-values [w h] (values (pict-width p) (pict-height p)))
  (define color (or pre-color "white"))
  (define bg
    (filled-rounded-rectangle (+ w x-margin)
                              (+ h y-margin)
                              radius
                              #:color color
                              #:draw-border? draw-border?))
  (cc-superimpose bg p))

(define (add-tax base tax)
  (+ base (* base tax)))

(define (add-rounded-border pp
                            #:radius [radius 10]
                            #:background-color [pre-bg-color #f]
                            #:x-margin [x-margin 0]
                            #:y-margin [y-margin 0]
                            #:frame-width [frame-width 1]
                            #:frame-color [pre-frame-color #f])
  (define-values [w h] (values (pict-width pp) (pict-height pp)))
  (define frame-color (or pre-frame-color black))
  (define frame (rounded-rectangle (+ w x-margin)
                                   (+ h y-margin)
                                   radius
                                   #:border-width frame-width
                                   #:border-color frame-color))
  (define bg-color (or pre-bg-color "white"))
  (define pp/bg
    (add-rectangle-background pp
                              #:color bg-color
                              #:draw-border? #false
                              #:x-margin x-margin
                              #:y-margin y-margin
                              #:radius radius))
  (cc-superimpose pp/bg frame))

(define (add-spotlight-background pp
                                  #:blur [blur-val 15]
                                  #:color [pre-color #f]
                                  #:border-color [pre-border-color #f]
                                  #:border-width [border-width 10]
                                  #:x-margin [x-margin 40]
                                  #:y-margin [y-margin 40])
  (define border-color (or pre-border-color (string->color% "plum")))
  (define color (or pre-color border-color))
  (define e-body
      (filled-ellipse (+ x-margin (pict-width pp))
                      (+ y-margin (pict-height pp))
                      #:color color
                      #:draw-border? #false))
  (define e-border
      (ellipse (+ x-margin (pict-width pp))
               (+ y-margin (pict-height pp))
               #:border-color border-color
               #:border-width border-width))
  (define e-bb
    (blur e-border blur-val blur-val))
  (cc-superimpose e-bb e-body pp))

(define (tag-append . x*)
  (string->symbol (string-join (map ~a x*) "-")))

(define (add-hubs pp tag
                  #:hub-length [pre-hub-len #f]
                  #:margin [pre-hub-gap #f])
  (define hub-len (or pre-hub-len 8))
  (define hub-gap (or pre-hub-gap 6))
  (define h-blank (blank 0 hub-len))
  (define v-blank (blank hub-len 0))
  (vc-append
    hub-gap
    (tag-pict v-blank (tag-append tag 'N))
    (hc-append
      hub-gap
      (tag-pict h-blank (tag-append tag 'W))
      (tag-pict pp tag)
      (tag-pict h-blank (tag-append tag 'E)))
    (tag-pict v-blank (tag-append tag 'S))))

;; -----------------------------------------------------------------------------
;; constructors

(define (xblank n)
  (blank n 0))

(define (yblank n)
  (blank 0 n))

(define (pblank pp)
  (blank (pict-width pp) (pict-height pp)))

(define (make-envelope-pict ww hh
                            #:color [color #f]
                            #:line-width [line-width #f]
                            #:line-color [line-color #f])
  (define (draw dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush (new brush% [style 'solid] [color (or color "mint cream")]))
    (send dc set-pen (new pen% [width (or line-width 2)] [color (or line-color "black")]))
    (define path (new dc-path%))
    (send path rectangle 0 0 ww hh)
    (send dc draw-path path dx dy)
    (send dc draw-lines `((0 . 0) (,(* ww 1/2) . ,(* hh 6/10)) (,ww . 0)) dx dy)
    (send dc set-brush old-brush)
    (send dc set-pen old-pen))
  (dc draw ww hh))

(define (make-compass-pict wh #:color [color black])
  (define border-color
    ;; TODO need to change drawing method before we can parameterize border-color; for now the border color appears "inside" the pict
    color)
  (define (draw dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush (new brush% [style 'solid] [color color]))
    (send dc set-pen (new pen% [width 1] [color border-color]))
    ;; ---
    (define wh2 (* 1/2 wh))
    (define wh4 (* 35/100 wh))
    (define wh6 (* 65/100 wh))
    (for* ((a? (in-list '(#t #f)))
           (b? (in-list '(#t #f))))
      (define path (new dc-path%))
      (cond
        [(and a? b?)
         (send path move-to wh2 0)]
        [(and a? (not b?))
         (send path move-to wh2 wh)]
        [(and (not a?) b?)
         (send path move-to 0 wh2)]
        [else
         (send path move-to wh wh2)])
      (if a?
        (begin
          (send path line-to wh4 wh2)
          (send path line-to wh6 wh2))
        (begin
          (send path line-to wh2 wh4)
          (send path line-to wh2 wh6)))
      (send path close)
      (send dc draw-path path dx dy))
    ;; ---
    (send dc set-brush old-brush)
    (send dc set-pen old-pen))
  (dc draw wh wh))

(define (make-mouse-cursor-pict w h #:color [color black])
  (rotate
    (ct-superimpose
      (vc-append (* 42/100 h) (blank) (rule (* 17/100 w) (* 1/2 h) #:color color))
      (colorize (arrowhead (min w h) (* revolution 1/4)) color))
    (* 1/14 revolution)))

(define (make-simple-flag flag-pict
                          #:flag-background-color [pre-flag-background-color #f]
                          #:flag-border-color [pre-flag-border-color #f]
                          #:flag-border-width [pre-flag-border-width #f]
                          #:flag-brush-style [pre-flag-brush-style #f]
                          #:flag-x-margin [pre-x-margin #f]
                          #:flag-y-margin [pre-y-margin #f]
                          #:pole-width [pre-pole-width #f]
                          #:pole-height [pre-pole-height #f]
                          #:pole-color [pre-pole-color #f]
                          #:pole-border-color [pre-pole-border-color #f]
                   )
  (define flag
    (let* ([flag-background-color (or pre-flag-background-color (hex-triplet->color% #xFFAA5B))]
           [flag-border-color (or pre-flag-border-color (hex-triplet->color% #x697F4D))]
           [flag-border-width (or pre-flag-border-width 5)]
           [flag-brush-style (or pre-flag-brush-style 'crossdiag-hatch)]
           [x-margin (or pre-x-margin 20)]
           [y-margin (or pre-y-margin 15)]
           [w (let ((pw (pict-width flag-pict))) (+ pw (* 5/100 pw) (* 2 x-margin)))]
           [h (+ (pict-height flag-pict) (* 2 y-margin))]
           [h/2 (* 1/2 h)]
           [w/offset (* 92/100 w)])
      (define (draw-flag dc dx dy)
        (define old-brush (send dc get-brush))
        (define old-pen (send dc get-pen))
        (send dc set-pen (new pen% [width flag-border-width] [color flag-border-color]))
        (for ((brush (in-list
                       (list
                         (new brush% [style 'solid] [color white])
                         (new brush% [style flag-brush-style] [color flag-background-color])))))
          (send dc set-brush brush)
          (define path (new dc-path%))
          (send path move-to w 0)
          (send path line-to 0 0)
          (send path line-to 0 h)
          (send path line-to w h)
          (send path line-to w/offset h/2)
          (send path line-to w 0)
          (send path close)
          (send dc draw-path path dx dy))
        ;; --
        (send dc set-brush old-brush)
        (send dc set-pen old-pen))
      (lc-superimpose (dc draw-flag w h) (ht-append (* 3/4 x-margin) (blank) flag-pict))))
  (define pole
    (let* ([w (or pre-pole-width 5)]
           [h (or pre-pole-height (* 2 (pict-height flag)))]
           [pole-radius 2]
           [pole-color (or pre-pole-color (string->color% "Brown"))]
           [pole-border-color (or pre-pole-border-color pole-color)])
      (filled-rounded-rectangle w h pole-radius
                                #:color pole-color
                                #:border-color pole-border-color
                                #:border-width 1)))
  (lt-superimpose (ht-append 4 (blank) pole) (vl-append 7 (blank) flag)))

(define (ptable kv**
                #:ncols [ncol #f]
                #:col-sep [col-sep #f]
                #:row-sep [row-sep #f]
                #:col-align [col-align #f]
                #:row-align [row-align #f])
  (table (or ncol (infer-num-columns kv** 'ptable) 2)
         (flatten kv**)
         (or col-align lc-superimpose)
         (or row-align cc-superimpose)
         (or col-sep 20)
         (or row-sep 10)))

(define (infer-num-columns pict*0 who)
  (if (or (null? pict*0)
          (not (pair? (car pict*0))))
    #f
    (let ((len (length* (car pict*0))))
      (for  ((pict* (in-list (cdr pict*0)))
             (ii (in-naturals 1)))
        (define l2 (length* pict*))
        (unless (= len l2)
          (raise-arguments-error who
                                 (format "(listof (list of ~a items))" len)
                                 "bad count" l2
                                 "bad position" ii
                                 "original" pict*0)))
      len)))

(define (length* x0)
  (let loop ((xx x0))
    (if (pair? xx)
      (+ 1 (if (pair? (cdr xx))
             (loop (cdr xx))
             1))
      0)))

(define (make-check-pict size #:color [color #f] #:line-color [line-color #f] #:line-width [-line-width #f])
  (define outer-color (or line-color (hex-triplet->color% #x598F61)))
  (define inner-color (or color (hex-triplet->color% #x71BE8D)))
  ;;
  (define size/2 (/ size 2))
  (define size/3 (/ size 3))
  (define line-width (or -line-width (/ size 6)))
  (define line-width/2 (/ line-width 2))
  (define F 50/100)
  ;;
  (define (draw-check dc% dx dy)
    (define old-brush (send dc% get-brush))
    (define old-pen (send dc% get-pen))
    ;;
    (send dc% set-brush (new brush% [color inner-color]))
    (send dc% set-pen (new pen% [width 1] [color outer-color]))
    ;; draw check from mid-left
    (define path% (new dc-path%))
    (send path% move-to (+ line-width/2 2) (* F size))
    (send path% line-to (- size/2 (/ line-width 2)) size)
    (send path% line-to (+ size/2 (/ line-width 4)) size)
    (send path% line-to size 0)
    (send path% line-to (- size line-width) 0)
    (send path% line-to (- size/2 (/ line-width 8)) (- size line-width))
    (send path% line-to (- size/2 (/ line-width 5)) (- size line-width))
    (send path% line-to (+ (* 1.6 line-width) 2) (* F size))
    (send path% close)
    (send dc% draw-path path% dx dy)
    ;;
    (send dc% set-brush old-brush)
    (send dc% set-pen old-pen)
    (void))
  (dc draw-check size size))

(define (make-cross-pict size #:color [color #f] #:line-color [line-color #f] #:line-width [-line-width #f])
  (define outer-color (or line-color (hex-triplet->color% #xC3476F)))
  (define inner-color (or color (hex-triplet->color% #xF0749C)))
  ;;
  (define size/2 (/ size 2))
  (define line-width (or -line-width (/ size 6)))
  (define line-width/2 (/ line-width 2))
  ;;
  (define (draw-x dc% dx dy)
    (define old-brush (send dc% get-brush))
    (define old-pen (send dc% get-pen))
    ;;
    (send dc% set-brush (new brush% [color inner-color]))
    (send dc% set-pen (new pen% [width 1] [color outer-color]))
    ;; draw X from top-left, counterclockwise
    (define path% (new dc-path%))
    (send path% move-to 0 0)
    (send path% line-to (- size/2 line-width/2) size/2)
    (send path% line-to 0 size)
    (send path% line-to line-width size)
    (send path% line-to size/2 (+ size/2 line-width/2))
    (send path% line-to (- size line-width) size)
    (send path% line-to size size)
    (send path% line-to (+ size/2 line-width/2) size/2)
    (send path% line-to size 0)
    (send path% line-to (- size line-width) 0)
    (send path% line-to size/2 (- size/2 line-width/2))
    (send path% line-to line-width 0)
    (send path% close)
    (send dc% draw-path path% dx dy)
    ;;
    (send dc% set-brush old-brush)
    (send dc% set-pen old-pen)
    (void))
  (dc draw-x size size))

(define (make-font-table-pict str #:size [pre-size #f] #:limit [limit #f])
  (define size (or pre-size 14))
  (table
    2
    (for/fold ([acc '()]
               #:result (reverse acc))
              ([face (in-list (get-face-list))]
               [_i (if limit (in-range limit) (in-naturals))])
      (list* (text str face size) (text face '() size) acc))
    lb-superimpose lb-superimpose 20 5))

(define (clip-to pp w h)
  (clip
    (ppict-do
      (blank w h)
      #:go (coord 1/2 1/2 'cc) pp)))

(define bbox-radius (make-parameter 1))
(define bbox-x-margin (make-parameter 66))
(define bbox-y-margin (make-parameter 14))
(define bbox-frame-width (make-parameter 2))
(define bbox-frame-color (make-parameter (hex-triplet->color% #x002E6D)))

(define (bbox pp
              #:color [color white]
              #:x-margin [x-margin #f]
              #:y-margin [y-margin #f]
              #:frame-color [frame-color #f]
              #:frame-width [frame-width #f]
              #:backup? [backup? #f])
  (define xm (or x-margin (bbox-x-margin)))
  (define ym (or y-margin (bbox-y-margin)))
  (define rr (bbox-radius))
  (add-rounded-border
    (if backup?
      (add-rounded-border
        pp
        #:x-margin xm #:y-margin ym #:radius rr
        #:background-color color #:frame-width 0)
      pp)
    #:x-margin (if backup? 0 xm)
    #:y-margin (if backup? 0 ym)
    #:radius rr
    #:background-color (if backup? white color)
    #:frame-width (or frame-width (bbox-frame-width))
    #:frame-color (or frame-color (bbox-frame-color))))

(define (sbox pp)
  (define pico-y-sep 7)
  (bbox pp
        #:x-margin pico-y-sep
        #:y-margin pico-y-sep))

;; =============================================================================

(module+ test
  (require rackunit)

  (test-case "edge case"
    (check-equal? (pict-bbox-sup) '())
    )

)
