#lang racket/base

(require
  pict
  ppict/pict
  ppict/tag
  pict-abbrevs)

(provide
  trace-pict
  )

;; ---

(define turn revolution)

(define neutral-border (hex-triplet->color% #x565656))
(define off-white (hex-triplet->color% #xeeeeee))
(define neutral-bg (hex-triplet->color% #xf9f9f9))
(define wong-red (hex-triplet->color% #xd55e00))
(define wong-green (hex-triplet->color% #x009e73))
(define wong-blue (hex-triplet->color% #x56B4E9))

(define code-font "Inconsolata")
(define code-size 32)

(define (coderm str)
  (text str code-font code-size))

(define (codebf str)
  (text str (cons 'bold code-font) code-size))

(define (trace-pict sym** #:index-labels? [index-labels? #true] #:lasso? [lasso? #true])
  (make-lasso* #:num? index-labels? #:lasso? lasso? (map ->panel sym**)))

(define (make-lasso . pp*)
  (make-lasso* pp*))

(define (make-lasso* pre-pp* #:lasso? [lasso? #true] #:num? [num? #true])
  ;; (frame (blank 1000 100))
  (define x-sep 12)
  (define y-sep 18)
  (define small-text coderm)
  (define pp*
    (for/list ((pp (in-list pre-pp*))
               (i (in-naturals)))
      (add-hubs pp (string->symbol (number->string i)))))
  (define num-pp* (length pp*))
  (let* ((pp (apply ht-append (* 4 x-sep) pp*))
         (pp (vl-append 0
                        (blank 0 (* 2 y-sep))
                        (ht-append 0 pp (blank (* 2 x-sep) 0))
                        (blank 0 (* 1/2 y-sep))))
         (pp (for/fold ((acc pp))
                       ((_ (in-list (cdr pre-pp*)))
                        (i (in-naturals)))
               (add-h-arrow acc
                            (format "~a-E" i)
                            (format "~a-W" (+ i 1)))))
         (pp (if lasso?
               (let ((i (sub1 (length pp*))))
                 (add-h-arrow pp (format "~a-E" i) (format "~a-W" i) #true))
               pp)))
    (if num?
      (for/fold ((pp pp))
                ((tgt (in-list pp*))
                 (i (in-naturals 1)))
        (ppict-do pp #:go (at-find-pict tgt cb-find 'ct #:abs-y -5)
                  (small-text (format "~a~a" i (if (and lasso? (= i num-pp*)) "+" "")))))
      pp)))

(define (->panel sym*)
  (cond
    [(string? sym*)
     (list->panel (map (compose1 string->symbol string) (string->list sym*)))]
    [(list? sym*)
      (list->panel sym*)]
    [else
      (raise-argument-error '->panel "(or/c string? (listof symbol?))" sym*)]))

(define (list->panel sym*)
  (make-panel (memq 'R sym*)
              (memq 'G sym*)
              (memq 'B sym*)))

(define (make-panel r-on g-on b-on)
  (define pp*
    (for/list ((sym (in-list '(R G B)))
               (on? (in-list (list r-on g-on b-on))))
      (make-light sym on?)))
  #;(rectangle-panel pp*)
  (circle-panel pp*))

(define (circle-panel pp*)
  (define light
    (vc-append 2
      (car pp*)
      (apply hc-append 6 (cdr pp*))))
  (ppict-do
    (disk
      (* 1.3 (max (pict-width light) (pict-height light)))
      #:draw-border? #true
      #:border-width 2
      #:border-color neutral-border
      #:color off-white)
    #:go (coord 1/2 45/100 'cc)
    light))

(define (make-light sym on?)
  (define base-color (symbol->color sym))
  (define bg
    (disk 30
          #:draw-border? #true
          #:color (if on? (color-off base-color) neutral-bg)
          #:border-color (if on? (color-on base-color) neutral-border)
          #:border-width (if on? 6 1)))
  (ppict-do
    bg
    #:go (coord 25/100 00/100 'lt #:abs-y -2)
    (make-label sym #:letter? #true #:on? #true)))

(define (symbol->word sym)
  (case sym
    ((R) "Red")
    ((G) "Green")
    ((B) "Blue")
    (else (raise-argument-error 'symbol->word "(or/c R G B)" sym))))

(define (symbol->color sym)
  (case sym
    ((R) wong-red)
    ((G) wong-green)
    ((B) wong-blue)
    (else (raise-argument-error 'symbol->color "(or/c R G B)" sym))))

(define (add-h-arrow pp pre-src pre-tgt [loop? #false])
  (define src (->symbol pre-src))
  (define tgt (->symbol pre-tgt))
  (pin-arrow-line
    10 pp
    (find-tag pp src)
    rc-find
    (find-tag pp tgt)
    (if loop? lt-find lc-find)
    #:line-width 3
    #:style 'solid
    #:start-angle (if loop? (* 18/100 turn) 0)
    #:end-angle (if loop? (* 80/100 turn) 0)
    #:start-pull (if loop? 90/100 0)
    #:end-pull (if loop? 75/100 0)
    #:color neutral-border))

(define (->symbol x)
  (cond
    [(symbol? x)
     x]
    [(string? x)
     (string->symbol x)]
    [(real? x)
     (->symbol (number->string x))]
    [else
      (error 'death)]))

(define (color-off c)
  (color%-update-alpha c 0.2))

(define (color-on c)
  c)

(define (symbol->letter sym)
  (case sym
    ((R) "R")
    ((G) "G")
    ((B) "B")
    (else (raise-argument-error 'symbol->letter "(or/c R G B)" sym))))

(define make-label
  (let* ((big-text codebf)
         (small-text coderm)
         (pp* (for*/list ((sym (in-list '(R G B)))
                          (on? (in-list '(#t #f))))
                ((if on? big-text small-text) (symbol->letter sym))))
         (pp* (list->vector (pict-bbox-sup* pp*))))
    (lambda (sym #:letter? [letter? #f] #:on? [on? #f])
      (if letter?
        (vector-ref pp* (+ (case sym ((R) 0) ((G) 2) ((B) 4)) (if on? 0 1)))
        ((if on? big-text small-text) (symbol->word sym))))))


