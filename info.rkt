#lang info
(define collection "pict-abbrevs")
(define deps '("base" "pict-lib" "lang-file" "draw-lib" "slideshow-lib" "ppict"))
(define build-deps '("rackunit-lib" "racket-doc" "scribble-doc" "gui-doc" "pict-doc" "draw-doc" "plot-doc" "plot-lib" "scribble-lib" "slideshow-doc"))
(define pkg-desc "Extra pict functions and a command-line tool for the pict library")
(define version "0.12")
(define pkg-authors '(ben))
(define scribblings '(("scribblings/pict-abbrevs.scrbl" () (tool-library))))
(define raco-commands '(("pict" (submod pict-abbrevs/private/raco main) "Command-line pict functions" #f)))
